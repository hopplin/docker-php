# Docker based PHP images

Docker images built on top of the official PHP images with the addition of some common and useful extensions.
Following images included:

- `PHP 7.1`
  _additional apps_: nodejs, yarn, goss, curl
  _active modules:_ pcntl, bcmath, bz2, calendar, iconv, intl,gd ,ldap, mbstring, memcached, mysqli, pdo_mysql, pdo_pgsql, pgsql, redis, soap, Zend OPcache, zip, redis, apcu, imap, xml, xmlrpc, xdebug, exif, imagick
- `PHP 7.2`
  _additional apps_: nodejs, yarn, goss, curl
  _active modules:_ pcntl, bcmath, bz2, calendar, iconv, intl,gd ,ldap, mbstring, memcached, mysqli, pdo_mysql, pdo_pgsql, pgsql, redis, soap, Zend OPcache, zip, redis, apcu, imap, xml, xmlrpc, xdebug, exif, imagick
- `PHP 7.3`
  _additional apps_: nodejs, yarn, goss, curl
  _active modules:_ pcntl, bcmath, bz2, calendar, iconv, intl,gd ,ldap, mbstring, memcached, mysqli, pdo_mysql, pdo_pgsql, pgsql, redis, soap, Zend OPcache, zip, redis, apcu, imap, xml, xmlrpc, xdebug, exif, imagick
- `MariaDB 10.3`
- `nginx 1.15`
- `alpine Linux 3.8` (plain)
  _additional apps_: curl
- `redis 5`