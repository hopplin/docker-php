CHANGELOG
=========

1.0.1 (open)
------------

* initial revision of redis-server in version 5
* php 7.3 alpine with xdebug included

1.0.0 (30.08.2018)
------------------

* Initial revision with PHP 7.2 alpine development Docker image
* Initial revision of PHP 7.1 alpine development Docker image
* Initial revision of PHP 7.3 alpine development Docker image
* Initial revision of MariaDB 10.3 Docker image
* Initial revision of alpine Linux (plain) Docker image
* Initial revision of nginx Docker image