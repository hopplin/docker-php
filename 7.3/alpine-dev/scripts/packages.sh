#!/bin/bash

apk update && apk upgrade

apk add --no-cache \
    curl \
    git \
    gnupg \
    grep \
    openssh-client \
    rsync \
    sudo \
    file \
    ca-certificates \
    python \
    gcc \
    g++ \
    make \
    zip \
    libressl-dev

apk add --no-cache --virtual .build-deps build-base autoconf

rm -rf /usr/share/man